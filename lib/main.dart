import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:shoppingGetx/views/shopping-page/shopping-page.view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: ShoppingPage(),
    );
  }
}
