import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:shoppingGetx/models/product-cart.model.dart';
import 'package:shoppingGetx/models/product.model.dart';
import 'package:shoppingGetx/controllers/shopping.controller.dart';
import 'package:shoppingGetx/utilitys/general.utility.dart';
import 'package:shoppingGetx/views/shopping-page/shopping-page.view.dart';

class Cart extends StatefulWidget {
  final ProductModel product;
  Cart({Key key, this.product}) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  bool isRerender = false;
  int totalMoney = 0;
  GeneralUtility generalUtility = GeneralUtility();
  final shoppingController = Get.put(ShoppingController());
  final oCcy = NumberFormat("#,##0", "en_US");

  _getTotalMoney() {
    setState(() {
      totalMoney = shoppingController.productsInCart
          .fold(0, (sum, item) => sum + item.amount * item.product.price);
    });
  }

  _payMent() {
    shoppingController.payMent();
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ShoppingPage()));
    generalUtility.showNotification('Đặt Hàng Thành Công');
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    _getTotalMoney();
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(title: Text('Giỏ Hàng'), centerTitle: true),
            body: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: shoppingController.productsInCart
                  .map((element) => buildPoduct(element))
                  .toList(),
            )),
            bottomNavigationBar: Container(
                height: 100,
                color: Colors.blue[500],
                child: Column(children: [
                  Container(
                      width: width,
                      height: 50,
                      color: Colors.orange,
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: 'Thành Tiền: ',
                                style: TextStyle(fontSize: 18)),
                            TextSpan(
                                text: oCcy.format(totalMoney) + 'đ',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      )),
                  Container(
                      width: width,
                      color: Colors.blue[500],
                      child: FlatButton(
                        onPressed: () {
                          _payMent();
                        },
                        child: Text('Thanh Toán',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                      ))
                ]))));
  }

  Widget buildPoduct(ProductCartModel productCart) {
    return Container(
        height: 80,
        margin: EdgeInsets.only(bottom: 5, top: 10, left: 10, right: 10),
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey[300]))),
        child: Row(children: [
          Expanded(
              flex: 2,
              child: Container(
                  child: CachedNetworkImage(
                imageUrl: productCart.product.image[0],
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                placeholder: (context, url) => Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(strokeWidth: 1.5),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ))),
          Expanded(
              flex: 8,
              child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                child: Text(productCart.product.description,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.cyan[600],
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500))),
                            Container(
                                child: InkWell(
                                    onTap: () {
                                      shoppingController
                                          .deleteProduct(productCart.product);
                                      setState(() {
                                        isRerender = true;
                                      });
                                    },
                                    child:
                                        Icon(Icons.delete, color: Colors.red)))
                          ],
                        )),
                        Container(
                            child: Row(children: [
                          Expanded(
                              child: Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                    text: 'Giá: ',
                                    style: TextStyle(fontSize: 16)),
                                TextSpan(
                                    text:
                                        oCcy.format(productCart.product.price) +
                                            'đ',
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 16)),
                              ],
                            ),
                          )),
                          Row(children: [
                            InkWell(
                              onTap: () {
                                shoppingController
                                    .removeProductInCart(productCart.product);
                                setState(() {
                                  isRerender = true;
                                });
                              },
                              child: Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text('-')),
                            ),
                            Container(
                                width: 30,
                                height: 30,
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(right: 5, left: 5),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(3)),
                                child: Text(productCart.amount.toString())),
                            InkWell(
                              onTap: () {
                                shoppingController
                                    .addProductToCart(productCart.product);
                                setState(() {
                                  isRerender = true;
                                });
                              },
                              child: Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text('+')),
                            ),
                          ])
                        ])),
                      ])))
        ]));
  }
}
