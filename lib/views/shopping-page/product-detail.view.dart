import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import 'package:shoppingGetx/views/cart/cart.view.dart';
import 'package:shoppingGetx/models/product.model.dart';
import 'package:shoppingGetx/shares/product.view.dart';
import 'package:shoppingGetx/controllers/shopping.controller.dart';

class ProdcutDetail extends StatefulWidget {
  final ProductModel product;
  ProdcutDetail({Key key, this.product}) : super(key: key);

  @override
  _ProdcutDetailState createState() => _ProdcutDetailState();
}

class _ProdcutDetailState extends State<ProdcutDetail> {
  bool isShowAppBar = false;
  bool isRerender = false;
  final shoppingController = Get.put(ShoppingController());
  final oCcy = NumberFormat("#,##0", "en_US");
  final PageController controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxScrolled) {
                  return isShowAppBar ? <Widget>[silverAppBar()] : [];
                },
                body: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      if (scrollNotification.metrics.pixels > 60) {
                        setState(() {
                          isShowAppBar = true;
                        });
                      } else {
                        setState(() {
                          isShowAppBar = false;
                        });
                      }
                    },
                    child: Container(
                        color: Colors.grey[200],
                        child: SingleChildScrollView(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            buildProduct(),
                            buildOtherProduct(),
                          ],
                        ))))),
            bottomNavigationBar: Container(
                padding: EdgeInsets.all(0),
                margin: EdgeInsets.all(0),
                child: Row(children: [
                  Expanded(
                      child: Container(
                          color: Colors.cyan,
                          child: FlatButton(
                              onPressed: () {
                                shoppingController
                                    .addProductToCart(widget.product);
                                setState(() {
                                  isRerender = true;
                                });
                              },
                              child: Icon(FontAwesome.cart_plus,
                                  color: Colors.white)))),
                  Expanded(
                      child: Container(
                          color: Colors.orange[800],
                          child: FlatButton(
                            onPressed: () {},
                            child: Text('Mua Ngay',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20)),
                          )))
                ]))));
  }

  SliverAppBar silverAppBar() {
    return SliverAppBar(
      elevation: 0,
      backgroundColor: Colors.white.withOpacity(0.45),
      pinned: true,
      floating: true,
      snap: false,
      collapsedHeight: 60,
      leading: buildGoBack(),
      actions: [buildCart()],
    );
  }

  Widget buildProduct() {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
        color: Colors.white,
        padding: EdgeInsets.only(bottom: 10),
        child: Column(children: [
          Container(
              width: width,
              height: height * 0.5,
              child: PageView(
                  scrollDirection: Axis.horizontal,
                  controller: controller,
                  children: widget.product.image
                      .asMap()
                      .entries
                      .map((item) => Container(
                          height: double.infinity,
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.only(bottom: 10),
                          child: Stack(children: [
                            Positioned(
                                child: CachedNetworkImage(
                              imageUrl: item.value,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5)),
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) => Container(
                                alignment: Alignment.center,
                                child:
                                    CircularProgressIndicator(strokeWidth: 1.5),
                              ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            )),
                            Positioned(
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                  isShowAppBar
                                      ? Container()
                                      : Expanded(
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                              buildGoBack(),
                                              Expanded(child: Container()),
                                              buildCart()
                                            ])),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(
                                                right: 10, bottom: 10),
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                                    .withOpacity(0.5),
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                            child: Text(
                                                '${item.key + 1}/${widget.product.image.length}'))
                                      ]),
                                ]))
                          ])))
                      .toList())),
          buildInforProduct()
        ]));
  }

  Widget buildGoBack() {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
            width: 45,
            height: 45,
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 10, top: 10),
            decoration: BoxDecoration(
                color: Colors.grey[500].withOpacity(0.4),
                borderRadius: BorderRadius.circular(50)),
            child: Icon(MaterialIcons.arrow_back, color: Colors.white)));
  }

  Widget buildCart() {
    return InkWell(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Cart()));
        },
        child: Container(
            width: 45,
            height: 45,
            alignment: Alignment.center,
            margin: EdgeInsets.only(right: 10, top: 10),
            decoration: BoxDecoration(
                color: Colors.grey[500].withOpacity(0.4),
                borderRadius: BorderRadius.circular(50)),
            child: Stack(alignment: Alignment.center, children: [
              Positioned(
                  child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Icon(FontAwesome.cart_plus, color: Colors.white))),
              Positioned(
                  child: Container(
                      width: 25,
                      height: 25,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 15, bottom: 15),
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(50)),
                      child: Obx(() => Text(
                          shoppingController.totalProductInCart.toString(),
                          style: TextStyle(color: Colors.white)))))
            ])));
  }

  Widget buildInforProduct() {
    return Container(
        margin: EdgeInsets.only(left: 5, right: 5),
        child: Column(children: [
          Row(children: [
            Expanded(
                child: Text(widget.product.description,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold))),
            Container(
                width: 70,
                height: 70,
                padding: EdgeInsets.only(top: 10, bottom: 10),
                decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(5)),
                child: Column(children: [
                  Text(
                    widget.product.promotion.toString() + '%',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Text(
                    'Giảm',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  )
                ]))
          ]),
          Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(children: [
                Expanded(
                    child: Text(oCcy.format(widget.product.price) + 'đ',
                        style: TextStyle(
                            fontSize: 22,
                            color: Colors.orange,
                            fontWeight: FontWeight.w500))),
                Container(
                    margin: EdgeInsets.only(top: 5),
                    child: SmoothStarRating(
                        allowHalfRating: false,
                        onRated: (v) {},
                        starCount: 5,
                        rating: widget.product.rating,
                        size: 20,
                        isReadOnly: true,
                        color: Colors.green,
                        borderColor: Colors.green,
                        spacing: 0.0)),
              ]))
        ]));
  }

  Widget buildOtherProduct() {
    double width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        color: Colors.white,
        margin: EdgeInsets.only(top: 20),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              padding:
                  EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
              child: Text('Sản Phẩm khác',
                  style: TextStyle(
                      color: Colors.orange,
                      fontSize: 20,
                      fontWeight: FontWeight.w500))),
          Products()
        ]));
  }
}
