import 'package:flutter/material.dart';

import 'package:shoppingGetx/shares/product.view.dart';

class ShoppingPage extends StatefulWidget {
  ShoppingPage({Key key}) : super(key: key);

  @override
  _ShoppingPageState createState() => _ShoppingPageState();
}

class _ShoppingPageState extends State<ShoppingPage> {
  List<String> banner = [
    'https://sanpham2021.s3.ap-southeast-1.amazonaws.com/copy/2021/03/22150626/banner-khuyen-mai-tiki-6.png',
    'https://dailygiamgia.com/wp-content/uploads/2019/07/M%E1%BB%ABng-sinh-nh%E1%BA%ADt-Tiki-9-tu%E1%BB%95i.jpg',
    'https://s3.cloud.cmctelecom.vn/tinhte1/2018/03/4267082_CV.jpg'
  ];

  final PageController controller = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return SafeArea(
        child: Scaffold(
      body: Container(
        height: height,
        child: SingleChildScrollView(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [buildCarousel(), Products()],
        )),
      ),
    ));
  }

  Widget buildCarousel() {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        height: height * 0.25,
        child: PageView(
            scrollDirection: Axis.horizontal,
            controller: controller,
            children: banner
                .asMap()
                .entries
                .map((item) => Container(
                    height: double.infinity,
                    margin: EdgeInsets.all(5),
                    padding: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            offset: const Offset(1, 1),
                            blurRadius: 5),
                      ],
                      image: DecorationImage(
                          image: NetworkImage(item.value), fit: BoxFit.cover),
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: buildDot(item.key))))
                .toList()));
  }

  List<Widget> buildDot(int currentIndex) {
    return banner
        .asMap()
        .entries
        .map((e) => Container(
            width: 15,
            height: 15,
            margin: EdgeInsets.only(right: 10),
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
                color: currentIndex == e.key ? Colors.red : Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey,
                      offset: const Offset(1, 1),
                      blurRadius: 5),
                ])))
        .toList();
  }
}
