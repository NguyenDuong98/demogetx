import 'package:shoppingGetx/models/product.model.dart';

List<ProductModel> productDatabase = [
  ProductModel(
      id: '1',
      description:
          'Quạt mini tích điện để bàn có kẹp tiện lợi đường kính lồng quạt 13cm thiết kế xoay 360 độ pin chạy 3-4h dễ dàng mang đi',
      price: 135000,
      rating: 5,
      promotion: 20,
      image: [
        'https://cf.shopee.vn/file/bc9c962ce41ba39eb0ab77bc439dee25',
        'https://cf.shopee.vn/file/2919f4c1120dae5ec944bd56eb4fd151',
        'https://cf.shopee.vn/file/e9dc3ecab4ebcca30504767e027ad9c6',
        'https://cf.shopee.vn/file/1b25ec28adef74cbb5d811128ad587e6'
      ]),
  ProductModel(
      id: '2',
      description: 'JISULIFE Quạt cầm tay mini đầu USB đa năng 4800MAH',
      price: 180000,
      rating: 3.5,
      promotion: 36,
      image: [
        'https://cf.shopee.vn/file/f33a34d106b312fe2fa90c7379fc1f52',
        'https://cf.shopee.vn/file/b6f241bc95ed84a6c39b686bf01dc359',
        'https://cf.shopee.vn/file/170556e08441dc8cb1d1c28b249a8546',
        'https://cf.shopee.vn/file/d1523142be49e5366c7766879fd8eda6',
        'https://cf.shopee.vn/file/342edd0d49d76d9d76f9e3f1cde1ca0f'
      ]),
  ProductModel(
      id: '3',
      description: 'Chảo siêu bền đá SUNHOUSE SBD phi 24-30cm',
      price: 300000,
      rating: 3,
      promotion: 13,
      image: [
        'https://cf.shopee.vn/file/061c8f61b2c633f07dde553c9d4972aa',
        'https://cf.shopee.vn/file/8b47665f5faa4e6b5d1515a0757892f1',
        'https://cf.shopee.vn/file/6598a1d34975a16508c4be9da757d0f9'
      ]),
  ProductModel(
      id: '4',
      description:
          '[Mã ELMALL1TR giảm 5% đơn 3TR] Apple iPad Pro 11 inch (2021) M1, Wi-Fi',
      price: 23990000,
      rating: 4,
      promotion: 0,
      image: [
        'https://cf.shopee.vn/file/5031966d0a0fcad7bf3f1f6b022cbeaa',
        'https://cf.shopee.vn/file/ae0aff04b1821e98dca26fe580f2efa4',
        'https://cf.shopee.vn/file/7f71a9f13cdeee2473684ead56c74a08',
      ]),
  ProductModel(
      id: '5',
      description:
          'Bàn gấp nhỏ gọn thông minh có khe cắm Ipad, điện thoại cho học sinh, sinh viên, công chức, các bé mẫu giáo',
      price: 59000,
      rating: 4.5,
      promotion: 21,
      image: [
        'https://cf.shopee.vn/file/bcadc79d3f8695c9a33c1a3bf3da0dca',
        'https://cf.shopee.vn/file/ee77732b9cfafa57567ceff9370e14b3',
        'https://cf.shopee.vn/file/7f62d9fdd9d552a4ebd1383dd974d037',
        'https://cf.shopee.vn/file/3b703a03409951ad746c4cdf3df07548'
      ]),
  ProductModel(
      id: '6',
      description:
          'Thảm Lông Loang Trải Sàn Lông Dầy Hàng Đẹp 2020 ( Kích Thước 1m6 x 2m - Mặt Sau Chồng trơn trượt)',
      price: 150000,
      rating: 4.5,
      promotion: 42,
      image: [
        'https://cf.shopee.vn/file/914969325adae0485cb82fd8734c7b4b',
        'https://cf.shopee.vn/file/017f1524fe8087ee2bfdfa3d4c0949b3',
        'https://cf.shopee.vn/file/6a70fc31b50fc164220fea5e982cad20',
        'https://cf.shopee.vn/file/4a2beab5258efc4e8abccad53530ca4c'
      ]),
  ProductModel(
      id: '7',
      description: 'Bàn gấp chân cao 71cm sơn tĩnh điện siêu bền',
      price: 150000,
      rating: 5,
      promotion: 14,
      image: [
        'https://cf.shopee.vn/file/2d83539747159fc63b19abbfe93769d1',
        'https://cf.shopee.vn/file/805d8f172a480fdc10b577999b7099ae',
        'https://cf.shopee.vn/file/b3a79ec7b90596de0da5db70d624927f',
        'https://cf.shopee.vn/file/d6d606d0d1cde17e5fb90490bec742a6'
      ]),
  ProductModel(
      id: '8',
      description:
          'QUẠT CẦM TAY MINI HÌNH CON HEO ĐA NĂNG 3 TRONG 1 CÓ ĐÈN LED CÓ GƯƠNG ĐỂ SOI TIỆN LỢI CHO CÁC BẠN NỮ CỰC KUTE',
      price: 50000,
      rating: 4.5,
      promotion: 0,
      image: [
        'https://cf.shopee.vn/file/24e517383e2a44674ab3c02b0238c1fe',
        'https://cf.shopee.vn/file/67e4e2fe155224f5ad759842483955b7',
        'https://cf.shopee.vn/file/2e063bbca94b163472c5ee74b1a9d746',
        'https://cf.shopee.vn/file/9dccd039170355e522b46800f09d39bd'
      ])
];
