import 'package:get/get.dart';

import 'package:shoppingGetx/models/product-cart.model.dart';
import 'package:shoppingGetx/services/shopping.service.dart';
import 'package:shoppingGetx/models/product.model.dart';

class ShoppingController extends GetxController {
  var products = List<ProductModel>().obs;
  var productsInCart = List<ProductCartModel>().obs;
  int get totalProductInCart =>
      productsInCart.fold(0, (sum, item) => sum + item.amount);
  ShoppingServices shoppingServices = ShoppingServices();

  Future<List<ProductModel>> getProducts() async {
    final response = await shoppingServices.getProducts();
    products.assignAll(response);
    return response;
  }

  addProductToCart(ProductModel product) {
    final index = productsInCart
        .indexWhere((element) => element.product.id == product.id);
    if (index == -1) {
      ProductCartModel newProductCart =
          ProductCartModel(amount: 1, product: product);
      productsInCart.add(newProductCart);
    } else {
      productsInCart[index].amount += 1;
    }
  }

  removeProductInCart(ProductModel product) {
    final index = productsInCart
        .indexWhere((element) => element.product.id == product.id);
    if (productsInCart[index].amount > 1) {
      productsInCart[index].amount -= 1;
    } else {
      productsInCart.removeAt(index);
    }
  }

  deleteProduct(ProductModel product) {
    productsInCart.removeWhere((element) => element.product.id == product.id);
  }

  payMent() {
    productsInCart.assignAll([]);
  }
}
