// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product-cart.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductCartModel _$ProductCartModelFromJson(Map<String, dynamic> json) {
  return ProductCartModel(
    id: json['id'] as String,
    amount: json['amount'] as int,
    product: json['product'] as ProductModel,
  );
}

Map<String, dynamic> _$ProductCartModelToJson(ProductCartModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'amount': instance.amount,
      'product': instance.product
    };
