import 'package:shoppingGetx/models/product.model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'product-cart.model.g.dart';

@JsonSerializable(includeIfNull: true)
class ProductCartModel {
  String id;
  int amount;
  ProductModel product;

  ProductCartModel({this.id, this.amount, this.product});

  factory ProductCartModel.fromJson(Map<String, dynamic> json) =>
      _$ProductCartModelFromJson(json);
  Map<String, dynamic> toJson() => _$ProductCartModelToJson(this);
}
