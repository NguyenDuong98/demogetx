import 'package:json_annotation/json_annotation.dart';
part 'product.model.g.dart';

@JsonSerializable(includeIfNull: true)
class ProductModel {
  String id;
  int price;
  String description;
  double rating;
  int promotion;
  List<String> image;

  ProductModel(
      {this.id,
      this.description,
      this.price,
      this.rating,
      this.promotion,
      this.image});

  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);
  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}
