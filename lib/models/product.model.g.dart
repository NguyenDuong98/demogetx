// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductModel _$ProductModelFromJson(Map<String, dynamic> json) {
  return ProductModel(
      id: json['id'] as String,
      price: json['price'] as int,
      description: json['description'] as String,
      rating: json['rating'] as double,
      promotion: json['promotion'] as int,
      image: json['image'] as List<String>);
}

Map<String, dynamic> _$ProductModelToJson(ProductModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'price': instance.price,
      'rating': instance.rating,
      'promotion': instance.promotion,
      'image': instance.image
    };
