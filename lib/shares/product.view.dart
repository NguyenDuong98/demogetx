import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import 'package:shoppingGetx/database/product.database.dart';
import 'package:shoppingGetx/models/product.model.dart';
import 'package:shoppingGetx/controllers/shopping.controller.dart';
import 'package:shoppingGetx/views/shopping-page/product-detail.view.dart';

class Products extends StatefulWidget {
  Products({Key key}) : super(key: key);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  List<ProductModel> products = [];

  final oCcy = NumberFormat("#,##0", "en_US");
  final shoppingController = Get.put(ShoppingController());

  _getProduct() {
    if (shoppingController.products.isEmpty) {
      shoppingController.getProducts().then((value) => setState(() {
            products = value;
          }));
    } else {
      setState(() {
        products = shoppingController.products;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getProduct();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: products.isEmpty
            ? Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(strokeWidth: 1.5))
            : Wrap(
                children: productDatabase
                    .map((product) => buildProduct(product))
                    .toList()));
  }

  Widget buildProduct(ProductModel product) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProdcutDetail(product: product)));
        },
        child: Container(
            width: width * 0.5 - 10,
            height: height * 0.45,
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      offset: const Offset(1, 1),
                      blurRadius: 5),
                ]),
            child: Column(children: [
              Expanded(
                  flex: 7,
                  child: CachedNetworkImage(
                    imageUrl: product.image[0],
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                    placeholder: (context, url) => Container(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(strokeWidth: 1.5),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  )),
              Expanded(
                  flex: 3,
                  child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Text(
                                  product.description,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                )),
                            Container(
                                margin: EdgeInsets.only(top: 5),
                                child: SmoothStarRating(
                                    allowHalfRating: false,
                                    onRated: (v) {},
                                    starCount: 5,
                                    rating: product.rating,
                                    size: 20,
                                    isReadOnly: true,
                                    color: Colors.green,
                                    borderColor: Colors.green,
                                    spacing: 0.0)),
                            Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(children: [
                                  Expanded(
                                      child: Text(
                                    oCcy.format(product.price) + 'đ',
                                    style: TextStyle(
                                        color: Colors.orange[800],
                                        fontSize: 20),
                                  )),
                                  product.promotion == 0
                                      ? Container()
                                      : Container(
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: Text(
                                            '-' +
                                                product.promotion.toString() +
                                                '%',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ))
                                ]))
                          ])))
            ])));
  }
}
