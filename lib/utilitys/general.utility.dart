import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GeneralUtility {
  showNotification(String title) {
    Get.snackbar(
      '',
      '',
      snackPosition: SnackPosition.TOP,
      backgroundColor: Colors.white,
      titleText: Text(title),
      padding: EdgeInsets.only(top: 20, left: 15),
      duration: Duration(seconds: 2),
      boxShadows: [
        BoxShadow(
          color: Colors.grey,
          offset: Offset(0, 0),
          blurRadius: 6,
        ),
      ],
    );
  }
}
